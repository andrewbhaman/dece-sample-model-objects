//
//  ViewController.swift
//  Be Codable
//
//  Created by Kyle Lee on 9/2/17.
//  Copyright © 2017 Kyle Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let restClient = RestClient()
    
    @IBAction func onSendPostTapped() {
        let returnedRestaurants = restClient.searchRestaurants()
        
        print(returnedRestaurants)
    }

    @IBAction func onGetUsersTapped() {
        print("get")
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else { return }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, _, _) in
            guard let data = data else { return }
            
            do {
                let users = try JSONDecoder().decode([User].self, from: data)
                for user in users {
                    print(user.address.geo.lat)
                }

            } catch {}
        }
        task.resume()
    }
    
}

