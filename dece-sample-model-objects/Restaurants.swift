//
//  Restaurants.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

struct Restaurants: Encodable, Decodable {
   
    let yelpBusinessId: String?
    let businessHours: [BusinessHours]?
    let restaurantName: String?
    let averageCustomerRating: Double?
    let pricingLevel: String?
    let phoneNumber: String?
    let streetAddress: [String]?
    let latitude: Double?
    let longitude: Double?
    let yelpUrl: String?
    let photos: [String]?
    let transactions: String?
    let specialHours: [SpecialHoursDTO]?
    let didSwipeRight: String?
}
