//
//  User.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

struct User: Decodable {
    let address: Address
    let company: Company
    let email: String
    let id: Int
    let name: String
    let phone: String
    let username: String
    let website: String
}


struct Address: Decodable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
    let geo: Geo
    
}

struct Company: Decodable {
    let name: String
    let catchPhrase: String
    let bs: String
}

struct Geo: Decodable {
    let lat: String
    let lng: String
}

