//
//  RestClient.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

public class RestClient {
    
    public struct HttpConfig { static let dummyUrl = "https://jsonplaceholder.typicode.com/posts" }
        
    let newBusinessHours = BusinessHours(hoursType: "REGULAR", openNow: true, hours: ["Mon 11:00am - 2:00am", "Tue 11:00am - 2:00am", "Wed 11:00am - 2:00am", "Thu 11:00am - 2:00am", "Fri 11:00am - 2:00am", "Sat 11:00am - 2:00am", "Sun 11:00am - 2:00am"])
    
    let newBusinessHours2 = BusinessHours(hoursType: "REGULAR", openNow: true, hours: nil)
    
    var newRestaurants = Restaurants(yelpBusinessId: nil, businessHours: nil, restaurantName: nil, averageCustomerRating: nil, pricingLevel: nil, phoneNumber: nil, streetAddress: nil, latitude: nil, longitude: nil, yelpUrl: nil, photos: nil, transactions: nil, specialHours: nil, didSwipeRight: nil)
    
    func searchRestaurants() -> Restaurants {
        newRestaurants = Restaurants(yelpBusinessId: "VBNik92gQJxPLWsnAyiZow", businessHours: [newBusinessHours, newBusinessHours2], restaurantName: "Muddy Chicken", averageCustomerRating: 3, pricingLevel: "$$", phoneNumber: "(952) 582-1091", streetAddress: ["3120 State Hwy 13 W", "Burnsville, MN 55337"], latitude: 44.777372, longitude: -93.318711, yelpUrl: "https://www.yelp.com/biz/muddy-chicken-burnsville?adjust_creative=2k4dGdBpr2gsJq4DbCcLMA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=2k4dGdBpr2gsJq4DbCcLMA", photos: ["https://s3-media2.fl.yelpcdn.com/bphoto/OfOQmAwMN54fB0piotHPKw/o.jpg", "https://s3-media3.fl.yelpcdn.com/bphoto/lWBqi4tN92U4n9WPV_p4Kw/o.jpg", "https://s3-media2.fl.yelpcdn.com/bphoto/pvQDjJIRrmOlfXcZlZs_hg/o.jpg"], transactions: "delivery", specialHours: nil, didSwipeRight: nil)
        guard let url = URL(string: HttpConfig.dummyUrl) else { return newRestaurants }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do { let requestBody = try JSONEncoder().encode(newRestaurants); request.httpBody = requestBody } catch {}
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, _, _) in
            guard let data = data else { return }
            do { self.newRestaurants = try JSONDecoder().decode(Restaurants.self, from: data) } catch {} }
        
        task.resume()
        
        return self.newRestaurants
    }
}
