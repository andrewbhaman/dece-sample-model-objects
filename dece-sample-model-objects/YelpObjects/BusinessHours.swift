//
//  BusinessHours.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

struct BusinessHours: Encodable, Decodable {
    let hoursType: String?
    let openNow: Bool?
    let hours: [String]?
}
