//
//  SpecialHoursDTO.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

struct SpecialHoursDTO: Encodable, Decodable {
    let date: String?
    let isClosed: Bool?
    let hours: String?
    let isOvernight: Bool?
}
