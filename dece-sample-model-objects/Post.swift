//
//  Post.swift
//  Be Codable
//
//  Created by Andrew Haman on 10/2/19.
//  Copyright © 2019 Kyle Lee. All rights reserved.
//

import Foundation

struct Post: Encodable, Decodable {
    let body: String
    let id: Int
    let title: String
    let userId: Int
    let businessHours: [BusinessHours]?
}
